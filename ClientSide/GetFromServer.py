import boto3, botocore, paramiko, csv, sys, os
import xml.etree.cElementTree as ET
import tkinter as tk
import tkinter.ttk as ttk 
from HelperFunctions import XML_FILE_LOCATION, SERVER_FILES_PATH, OpenServerConnection, CloseServerConnection



class GetFromServer:
    def __init__(self, callBack, window, textbox):
        #Get files from server
        print("Getting files from server...")
        textbox.insert('1.0', "Getting files from server...\n")
        self.__GetFiles(textbox)
        #Put data into XML format
        print("Exporting data to XML...")
        textbox.insert("1.0", "Exporting data to XML...\n")
        self.__ToXML()
        print("Deleting local temp files...")
        textbox.insert("1.0", "Deleting local temp files...\n")
        files = os.listdir("./bin/")
        for f in files:
            if (f.endswith(".csv")):
                os.remove("./bin/" + f)
        print("Data Updated")
        textbox.insert("1.0", "Job Data Updated\n")
        print("Getting Blacklist words")
        textbox.insert("1.0", "Getting Blacklist words\n")
        self.__GetBlackListWords(textbox)
        print("Updated Blacklist words")
        textbox.insert("1.0", "Updated Blacklist words\n")
        callBack()

    def __GetFiles(self, textbox):
        sftp = OpenServerConnection()
        filesOnServer = sftp.listdir(SERVER_FILES_PATH() + "/bin")
        for f in filesOnServer:
            sftp.get(SERVER_FILES_PATH() + "/bin/" + f, "./bin/" + f)
            print("UPDATE: Temp file() " + f + " has been copied")
            textbox.insert("1.0", "UPDATE: Temp file() " + f + " has been copied\n")
            sftp.remove(SERVER_FILES_PATH() + "/bin/" + f)
        CloseServerConnection(sftp)

    def __ToXML(self):
        #Check if exists and get data to add
        jobs = None
        counter = 0
        if (os.path.exists(XML_FILE_LOCATION()) == True):
            tree = ET.parse(XML_FILE_LOCATION())
            jobs = tree.getroot()
        else:
            jobs = ET.Element("jobs")
        files = os.listdir("./bin/")
        for f in files:
            if (f.endswith(".csv")):
                with open("./bin/" + f) as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    for row in csv_reader:
                        job = ET.SubElement(jobs, "job")
                        rejected = ET.SubElement(job, "rejected")
                        jobtitle = ET.SubElement(job, "jobtitle")
                        category = ET.SubElement(job, "category")
                        location = ET.SubElement(job, "location")
                        company = ET.SubElement(job, "company")
                        contractType = ET.SubElement(job, "contractType")
                        pay = ET.SubElement(job, "pay")
                        description1 = ET.SubElement(job, "description1")
                        description2 = ET.SubElement(job, "description2")
                        postedDate = ET.SubElement(job, "postedDate")
                        url = ET.SubElement(job, "url")
                        rejectReason = ET.SubElement(job, "rejectReason")
                            
                        job.set('id', str(counter) + "-" + str(row[8]))
                        rejected.text = row[10]
                        jobtitle.text = row[0]
                        category.text = row[4]
                        location.text = row[2]
                        company.text = row[1]
                        contractType.text = "N/A"
                        pay.text = row[3]
                        description1.text = row[5]
                        description2.text = row[6]
                        postedDate.text = row[8]
                        url.text = row[9]
                        rejectReason.text = row[11]
                    counter += 1
        tree = ET.ElementTree(jobs)
        tree.write(XML_FILE_LOCATION())

    def __GetBlackListWords(self, textbox):
        sftp = OpenServerConnection()
        sftp.get(SERVER_FILES_PATH() + "/BlacklistWords", "./bin/BlacklistWords.txt")
        CloseServerConnection(sftp)