import tkinter as tk
import tkinter.ttk as ttk
import xml.etree.ElementTree as ET
import webbrowser, os
from HelperFunctions import XML_FILE_LOCATION, SERVER_FILES_PATH, OpenServerConnection, CloseServerConnection
from CreateTextTable import CreateTextTable
from PopOut import PopoutMsg

class Tab_RejectList:
    def __init__(self, window, tab):
        self.__window = window
        self.__CreateObjects(tab)
        self.__UpdateTree()

    def __CreateObjects(self, tab):
        self.__tree=ttk.Treeview(tab, height= self.__window.winfo_reqheight() - 175)
        self.__tree['show'] = 'headings'
        self.__tree.bind("<Double-1>", self.__OnDoubleClick)
        

        self.__tree["columns"] = ("1", "2", "3")

        self.__tree.column("1", width=366, stretch=tk.NO)
        self.__tree.column("2", width=40, stretch=tk.NO)
        self.__tree.column("3", width=90, stretch=tk.NO)

        self.__tree.heading("1", text="JobTitle")
        self.__tree.heading("2", text="Loc")
        self.__tree.heading("3", text="Posted Time")

        self.__buttonOpenLink = ttk.Button(tab, text="Open URL", command=self.__OpenURLCallBack)
        self.__buttonRemoveAll = ttk.Button(tab, text="Delete All", command=self.__RemoveItemsCallBack)

        self.__tree.grid(row = 1, column = 0, columnspan = 6, sticky = tk.N+tk.W, padx = 10, pady = 2)
        self.__buttonOpenLink.grid(row = 0, column = 0, sticky = tk.N+tk.W, padx = (10, 0), pady = 10)
        self.__buttonRemoveAll.grid(row = 0, column = 0, sticky = tk.N+tk.W, padx = (100, 0), pady = 10)

    def __UpdateTree(self):
        self.__treeRows = []
        tree = ET.parse(XML_FILE_LOCATION())
        jobsroot = tree.getroot()
        counter = 0
        for elem in jobsroot:
            if (elem[0].text == "True"):
                self.__treeRows.append([counter ,elem.attrib['id'], elem[0].text, elem[1].text, elem[2].text, elem[3].text, elem[4].text, elem[5].text,elem[6].text, elem[7].text, elem[8].text, elem[9].text, elem[10].text, elem[11].text])
                self.__tree.insert('', 'end', str(counter), values=(elem[1].text, elem[3].text, elem[9].text))
                counter += 1

    def __RemoveItemsCallBack(self):  
        for s in self.__tree.get_children():
            _id = None
            #Delete from tree row list
            for tr in self.__treeRows:
                if (tr[0] == int(s)):
                    _id = tr[1]
                    self.__treeRows.remove(tr)
                    break
            #Delete from XML
            tree = ET.parse(XML_FILE_LOCATION())
            jobsroot = tree.getroot()
            jobs = jobsroot.findall("job")
            for j in jobs:
                if (j.attrib["id"] == _id):
                    jobsroot.remove(j)
                    tree.write(XML_FILE_LOCATION())
                    break
            #Remove from Treeview
            self.__tree.delete(s)

    # EVENT FUNCTIONS 
        
    def __OnDoubleClick(self, event):
        headers = ["Tree ID", "XML ID", "Rejected", "Job Title", "Category", "Location", "Company", "Contract Type" ,"Pay", "Description 1", "Description 2", "Date", "URL", "Reject Reason"]
        PopoutMsg(CreateTextTable(headers, self.__treeRows[int(self.__tree.focus())], 27), "textbox" ,10 , 10, 490, 520)

    # BUTTON CALL BACK FUNCTIONS    

    def __OpenURLCallBack(self):
        webbrowser.open_new_tab("https://www.seek.com.au" + self.__treeRows[int(self.__tree.focus())][12])

class Tab_BlackListEditor:
    def __init__(self, window, tab, downloadOverride = False):
        self.window = window
        self.__CreateObjects(tab, downloadOverride)

    def __CreateObjects(self, tab, downloadOverride):
        self.textbox = tk.Text(tab, height = 20, width = 50)
        self.__ImportBlacklistWords()
        self.buttonOpenLink = ttk.Button(tab, text="Update", command=self.__UpdateCallBack)

        self.textbox.grid(row = 3, column = 0, sticky = tk.N+tk.W, padx = (10, 0), pady = 10)
        self.buttonOpenLink.grid(row = 3, column = 1, sticky = tk.N+tk.W, padx = (8, 0), pady = 10)

    def __UpdateCallBack(self):
        self.__UpdateBlackListWords()

    def __ImportBlacklistWords(self):
        if (os.path.exists("./bin/BlacklistWords.txt")):
            self.textbox.insert("1.0",  open("./bin/BlacklistWords.txt").read())
            os.remove("./bin/BlacklistWords.txt")
        else:
            print("WARNING: Blacklist file didn't exist on load")
            self.textbox.insert("1.0", "Wasn't able to get blacklist")
    
    def __UpdateBlackListWords(self):
        file = open("./bin/BlacklistWords.txt", "w")
        file.write(self.textbox.get("1.0", "end"))
        file.close()
        sftp = OpenServerConnection()
        sftp.put("./bin/BlacklistWords.txt", SERVER_FILES_PATH() + "/BlacklistWords")
        os.remove("./bin/BlacklistWords.txt")
        return

