from textwrap import wrap

def CreateTextTable(headers, inValues, length):
    charGap = 16
    itemCounter = 0
    firstLoop = True
    sepLine = "-" * (int(length) * 2 + 5)
    outStr = sepLine + "\n"
    for h in headers:
        firstLoop = True
        values = wrap(str(inValues[itemCounter]), length)
        for v in values:
            if firstLoop:
                firstLoop = False
                outStr += h + " " * int(charGap - len(h)) + "| " + v + "\n"
            else:
                outStr += " " * int(charGap) + "| " + v + "\n"
        outStr += sepLine + "\n"
        itemCounter += 1
        
    return outStr
