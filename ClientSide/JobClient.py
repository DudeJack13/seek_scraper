import tkinter as tk
import tkinter.ttk as ttk
from Tab1_RecentJobs import Tab_RecentJobs
from Tab2_BlackList_RejectList import Tab_RejectList, Tab_BlackListEditor
from LoadingWindow import LoadingWindow

class Main:
    def __init__(self):
        print("Seek Job Client v2.0")
        self.__LoadingMain()
        self.__LoadMain()
        
    def __LoadMain(self):
        self.__window = tk.Tk()
        self.__window.title("Job Client")
        self.__window.geometry('520x' + str(self.__window.winfo_screenheight() - 65) + "+0+0")
        self.__window.resizable(0, 1)

        tab_parent = ttk.Notebook(self.__window )
        tab1 = tk.Frame(tab_parent)
        tab2 = tk.Frame(tab_parent)

        tab_parent.add(tab1, text="Recent Jobs")
        tab_parent.add(tab2, text="Extra")
        tab_parent.pack(expand=1, fill='both')
        
        Tab_RecentJobs(self.__window , tab1)
        Tab_RejectList(self.__window , tab2)
        Tab_BlackListEditor(self.__window , tab2)

        print("Client loaded")
        self.__window.mainloop()

    def __LoadingMain(self):
        self.__lWindow = tk.Tk()
        self.__lWindow.title("Loading Job Client")
        self.__lWindow.geometry("520x350+0+0")
        self.__lWindow.resizable(0, 0)
        LoadingWindow(self.__lWindow)
        
if __name__ == "__main__": 
    app = Main()
