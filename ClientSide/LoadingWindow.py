import tkinter as tk
import tkinter.ttk as ttk
from GetFromServer import GetFromServer
from threading import Thread
import os

class LoadingWindow:
    def __init__(self, window):
        if not os.path.exists("./bin"):
            os.makedirs("./bin")
        self.__window = window
        self.__CreateObject(window)
        Thread(target = self.Load).start()
        self.__window.mainloop()

    def __ThreadCallBack(self):
        print("Loading Client\n")
        self.textbox.insert("1.0", "Loading Client\n")
        self.__window.destroy()
        return

    def Load(self):
        GetFromServer(self.__ThreadCallBack, self.__window, self.textbox)
        
    def __CreateObject(self, window):
        self.title = ttk.Label(window, text = "Seek Scraper v2.0")
        self.textbox = tk.Text(window, width = 63, height = 16)
        self.textbox.insert("1.0", "Loading Seek Job Client v2.0")

        self.title.grid(row = 1, column = 0, sticky = tk.N , padx = 5, pady = 5)
        self.textbox.grid(row = 3, column = 0,  sticky = tk.N+tk.W, padx = 5, pady = 5)