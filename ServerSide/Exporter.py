from Imports import csv
from datetime import datetime

def ExportData(goodJobs, badJobs, location):
    with open('./bin/JobData' + location + "_" + str(datetime.date(datetime.now())) + '.csv', mode='w') as job_file:
        print("Writing to CSV file")
        
        file_writer = csv.writer(job_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for j in goodJobs:
            file_writer.writerow([j["jobTitle"], j["companyName"], j["location"], j["pay"], j["category"], j["description1"], j["description2"], j["featured"], j["postedDate"], j["jobURL"], j["rejected"], j["rejectedReason"]])
        file_writer.writerow(["----------", "----------", "----------", "----------", "----------", "----------", "----------", "---------- ", "----------", "----------", "----------", "----------"])
        for j in badJobs:
            file_writer.writerow([j["jobTitle"], j["companyName"], j["location"], j["pay"], j["category"], j["description1"], j["description2"], j["featured"], j["postedDate"], j["jobURL"], j["rejected"], j["rejectedReason"]])

        job_file.close()
        print("JobData.csv exported")

